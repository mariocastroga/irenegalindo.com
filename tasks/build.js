'use strict';

var gulp = require('gulp');

gulp.task('build', [
  'templates',
  'styles',
  'scripts',
  'images',
  'php',
  'json',
  'fonts',
  'vendors'
]);
