'use strict';

var gulp = require('gulp');

gulp.task('fonts', function() {

    gulp.src("source/fonts/*.*")
        .pipe(gulp.dest("./website/fonts/"));
});
