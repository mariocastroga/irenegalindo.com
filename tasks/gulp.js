var gulp = require('gulp'),
    spawn = require('child_process').spawn,
    fs = require('fs');

gulp.task('gulp:reload', function() {
    var data = JSON.parse( fs.readFileSync('./env/dev.json', { encoding: 'utf8' }));
    var process;

    gulp.watch(data.watch.source, function () {
        if( process ) {
            process.kill();
        }

        process = spawn('gulp', { stdio: 'inherit' });
    });
});
