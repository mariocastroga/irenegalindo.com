'use strict';

var gulp = require('gulp');

gulp.task('images', function() {
    gulp.src([
        "source/images/**/*.gif",
        "source/images/**/*.jpg",
        "source/images/**/*.png",
        "source/images/**/*.svg",
        "source/images/**/*.mp4"
      ])
        .pipe(gulp.dest("./website/images"));
});
