'use strict';

var gulp = require('gulp'),
  concat = require('gulp-concat');

gulp.task('scripts', function() {
  gulp.src("source/scripts/*.js")
    .pipe(gulp.dest("./website/scripts"));
});
