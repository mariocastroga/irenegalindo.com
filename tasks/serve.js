'use strict';

var gulp = require('gulp'),
    browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
    browserSync({
        reloadDebounce: 500,
        reloadDelay: 500,
        port: 8080,
        server: {
            baseDir: './website'
        }
    });
});

gulp.task('serve', ['default', 'browser-sync'], function() {
    gulp.watch('source/**/*.{gif,jpg,png,svg}', ['images'], browserSync.reload);
    gulp.watch('source/**/*.pug', ['templates']).on('change', browserSync.reload);
    gulp.watch('source/**/*.styl', ['styles']).on('change', browserSync.reload);
    gulp.watch('source/**/*.js', ['scripts']).on('change', browserSync.reload);
    gulp.watch('source/**/*.php', ['php']).on('change', browserSync.reload);
    gulp.watch('source/**/*.json', ['json']).on('change', browserSync.reload);
});
