'use strict';

var gulp = require('gulp'),
    pug = require('gulp-pug');

gulp.task('templates', function() {
    gulp.src("source/templates/*.pug")
        .pipe(pug({
          pretty: true
        }))
        .pipe(gulp.dest('website'));

    gulp.src("source/templates/blog/*.pug")
        .pipe(pug({
          pretty: true
        }))
        .pipe(gulp.dest('website/blog'));
})
