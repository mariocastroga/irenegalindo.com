'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat');

gulp.task('vendors', function() {
    gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js'
      ])
        .pipe(concat('vendors.min.js'))
        .pipe(gulp.dest('website/scripts/'));

    gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'node_modules/bootstrap/dist/css/bootstrap.min.css.map',
        'node_modules/animate.css/animate.min.css',
        'node_modules/font-awesome/css/font-awesome.min.css'
      ])
        .pipe(concat("vendors.min.css"))
        .pipe(gulp.dest('website/styles/'));

    gulp.src([
        'node_modules/font-awesome/fonts/**',
        'node_modules/bootstrap/dist/fonts/**'
      ])
        .pipe(gulp.dest('website/fonts/'));
});
